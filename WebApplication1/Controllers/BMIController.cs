﻿using WebApplication1.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class BMIController : Controller
    {
        // GET: BMI
        public ActionResult Index()
        {
            return View(new BMIData());
        }

        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if (data.Height < 50 || data.Height > 200) {
                ViewBag.Herror = "身高請輸入50~200數值";
            }
            if (data.Weight < 30 || data.Weight > 150)
            {
                ViewBag.Werror = "體重請輸入30~150數值";
            }
            if (ModelState.IsValid) {
                float m_Height = data.Height / 100;
                float bmi = data.Weight / (m_Height * m_Height);

                string level = "";
                if (bmi<18.5) {
                    level = "過瘦";
                }
                else if (bmi > 18.5 && bmi< 24)
                {
                    level = "適中";
                }
                else if (bmi > 24)
                {
                    level = "過重";
                }
                data.BMI = bmi;
                data.Level = level;
            }
            

            return View(data);
        }
    }
}