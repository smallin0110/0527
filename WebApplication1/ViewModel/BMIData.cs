﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.ViewModel
{
    public class BMIData
    {
        [Required(ErrorMessage = "required filed")]
        [Range(30,150,ErrorMessage ="30~150")]
        public float Weight { get;  set; }

        [Required(ErrorMessage = "required filed")]
        [Range(50, 200, ErrorMessage = "50~200")]
        public float Height { get; set; }

        public float BMI { get; set; }
        public string Level { get; set; }
    }
}